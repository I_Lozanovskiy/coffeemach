public enum CoffeeTypes {

    ESPRESSO("для приготовления эспрессо необходимо: 30 мл воды и 5 грамм кофейных зерен."),
    CAPPUCCINO("для приготовления капучино необходимо: 200 мл воды, 10 грамм кофейных зерен и 20 мл молока"),
    IRISH(""),
    LATTE(""),
    ;
    private final String receipt;

   CoffeeTypes(String receipt){
        this.receipt = receipt;

    }
    public String getReceipt(){
       return receipt;
    }
}


