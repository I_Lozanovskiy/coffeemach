

import lombok.NonNull;
import lombok.extern.java.Log;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Scanner;
@Log

public class ComScanner extends Technics{
    Logger logger = new Logger();
    CoffeeMachine coffeeMachine = new CoffeeMachine();
    ArrayList<String> drinkLog = new ArrayList<>();
    HashMap<String, String> profiles = new HashMap<>();
    Scanner sc = new Scanner(System.in);
    @NonNull
    public void whiles() throws IOException {

        System.out.println("🐼Укажите объем подключенного бака для воды:🐼");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            int a = Integer.parseInt(reader.readLine());
            coffeeMachine.setMaxWater(a);
            System.out.println("🐼Укажите объем подключенного бака для молока:🐼");
            int b = Integer.parseInt(reader.readLine());
            coffeeMachine.setMaxMilk(b);
            System.out.println("🐼Укажите объем подключенного резервуара для кофе:🐼");
            int c = Integer.parseInt(reader.readLine());
            coffeeMachine.setMaxCoffee(c);
            System.out.println("🐼Не забудь включить машинку командой \"power on\" 🐼");
            System.out.println("🐼для вызова полного списка команд используй \"help\" 🐼");
        }
        catch (NumberFormatException ignored){
            System.out.println("🐼Введите коректные данные🐼");
            whiles();
        }
        //-----------------------------------//

        //-----------------------------------//

        while (sc.hasNext()) {
            String s = sc.nextLine();
            comScan(s);
        }
    }


    public void comScan(String s) {

        String[] parts = s.split(" ");
        String commandPart1 = parts[0];

        if (commandPart1.matches("help") && parts.length == 1) {
            System.out.println("Список команд🐼: ");
            System.out.println("Включение/выключение🐼: power on/off ");
            System.out.println("Добавление воды/молока/кофе [количество]🐼: add water/milk/coffee [amount] ");
            System.out.println("Прготовить эспрессо/капучино🐼: make espresso/cappuccino ");
            System.out.println("Проверить наполненность бака с водой/молоком/кофе,отходами 🐼: show water/milk/coffee/rubbish");
            System.out.println("Очистить бак с отходами🐼: clean rubbish");
            System.out.println("Приготовить эсперессо/капучино : 🐼make espresso/cappuccino🐼");
            System.out.println("Приготовить три кружки эсперессо 🐼want sleep🐼");
            System.out.println("Приготовить три кружки капучино 🐼want rest🐼");
            System.out.println("Приготовить эсперессо/капучино [количество кружек] 🐼 make espresso/cappuccino [count]🐼");
            System.out.println("Вывести на экран список выполненных команд 🐼print log🐼");
            System.out.println("Вывести на экран список приготовленных напитков 🐼print drinkLog🐼");
            System.out.println("🐼Вывести на экран рецепт эспрессо/капучино🐼: recipe espresso/cappuccino");
            System.out.println("🐼для создания профиля и сохранения списка напитков🐼: macros [имя профиля] далее вводится [список команд через , и пробел] ");
            System.out.println("🐼для вызова сохраненного профиля🐼 : use macros [имя профиля]");
            log.info("help");
            logger.addState("help");

        } else if (commandPart1.equalsIgnoreCase("print") && parts.length == 3) {
            String commandPart2 = parts[1];
            int commandPart3 = Integer.parseInt(parts[2]);
            if (commandPart2.equalsIgnoreCase("log")) {
                log.info("print only " + commandPart3 + " stripes of log");
                logger.getLogger(commandPart3);
            }

        } else if (commandPart1.equalsIgnoreCase("clock") && parts.length == 2) {
            String commandPart2 = parts[1];
            if (commandPart2.equalsIgnoreCase("on")) {
                log.info("clock on ");
                logger.addState("clock on");
                CoffeeMachine.clock();
                Technics.setEnable(true);
               // CoffeeMachine.clock(true);
            } else if (commandPart2.equalsIgnoreCase("off")) {
                //drinkLog.forEach(System.out::println);
                log.info("clock off ");
                logger.addState("clock off");
                Technics.setEnable(false);
                //CoffeeMachine.clock();


            }

        } else if (commandPart1.equalsIgnoreCase("print") && parts.length == 2) {
            String commandPart2 = parts[1];
            if (commandPart2.equalsIgnoreCase("log")) {
                log.info("print log ");
                logger.addState("print log");
                logger.getLogs();




            } else if (commandPart2.equalsIgnoreCase("drinkLog")) {
                drinkLog.forEach(System.out::println);
                log.info("print drink log ");
                logger.addState("print drink log ");

            }

        } else if (commandPart1.equalsIgnoreCase("power") && parts.length == 2) {
            String commandPart2 = parts[1];
            if (commandPart2.equalsIgnoreCase("on")) {
                coffeeMachine.powerOn();
                log.info("power on ");
                logger.addState("power on ");

            } else if (commandPart2.equalsIgnoreCase("off")) {
                coffeeMachine.powerOff();
                log.info("power off");
                logger.addState("power off");

            }
        } else if (commandPart1.equalsIgnoreCase("add") && parts.length == 3) {
            String commandPart2 = parts[1];
            double commandPart3 = Double.parseDouble(parts[2]);
            if (commandPart2.equalsIgnoreCase("water")) {
                coffeeMachine.addWater(commandPart3);
                log.info("add water " + commandPart3);
                logger.addState("add water " + commandPart3);

            } else if (commandPart2.equalsIgnoreCase("milk")) {
                coffeeMachine.addMilk(commandPart3);
                log.info("add milk " + commandPart3);
                logger.addState("add milk " + commandPart3);
            } else if (commandPart2.equalsIgnoreCase("coffee")) {
                coffeeMachine.addCoffee(commandPart3);
                log.info("add coffee " + commandPart3);
                logger.addState("add coffee " + commandPart3);

            } else
                System.out.println("🐼Enter correct command!🐼");


        } else if (commandPart1.equalsIgnoreCase("show") && parts.length == 2) {
            String commandPart2 = parts[1];
            if (commandPart2.matches("water")) {
                coffeeMachine.getWaterStatus();
                log.info("show water ");
                logger.addState("show water ");

            } else if (commandPart2.matches("milk")) {
                coffeeMachine.getMilkStatus();
                log.info("show milk");
                logger.addState("show milk");

            } else if (commandPart2.matches("coffee")) {
                coffeeMachine.getCoffeeStatus();
                log.info("show coffee");
                logger.addState("show coffee");

            } else if (commandPart2.matches("rubbish")) {
                coffeeMachine.getRubbishStatus();
                log.info("show rubbish");
                logger.addState("show rubbish");

            }
        } else if (commandPart1.equalsIgnoreCase("make") && parts.length == 2) {
            String commandPart2 = parts[1];
            if (commandPart2.matches("espresso")) {
                coffeeMachine.isCleanNeed();
                coffeeMachine.espresso();
                Date date30 = new Date();
                drinkLog.add("espresso " + date30);
                log.info("make espresso");
                logger.addState("make espresso");

            } else if (commandPart2.matches("cappuccino")) {
                coffeeMachine.isCleanNeed();
                coffeeMachine.cappuccino();
                Date date31 = new Date();
                drinkLog.add("cappuccino " + date31);
                log.info("make cappuccino");
                logger.addState("make cappuccino");

            }
        } else if (commandPart1.equalsIgnoreCase("clean") && parts.length == 2) {
            String commandPart2 = parts[1];
            if (commandPart2.equalsIgnoreCase("rubbish")) {
                coffeeMachine.cleanRubbish();
                log.info("clean rubbish");
                logger.addState("clean rubbish");
            }
        } else if (commandPart1.equalsIgnoreCase("make") && parts.length == 3) {
            String commandPart2 = parts[1];
            int commandPart3 = Integer.parseInt(parts[2]);
            if (commandPart2.equalsIgnoreCase("cappuccino")) {
                coffeeMachine.isCleanNeed();
                coffeeMachine.manyCappuccino(commandPart3);
                Date date32 = new Date();
                drinkLog.add(commandPart3 + " cups of cappuccino " + date32);
                log.info("make" + commandPart3 + " cups of cappuccino ");
                logger.addState("make" + commandPart3 + " cups of cappuccino ");
            } else if (commandPart2.equalsIgnoreCase("espresso")) {
                coffeeMachine.isCleanNeed();
                coffeeMachine.manyEspresso(commandPart3);
                Date date33 = new Date();
                drinkLog.add(commandPart3 + " cups of espresso " + date33);
                log.info( "make " + commandPart3 + " cups of espresso ");
                logger.addState("make " + commandPart3 + " cups of espresso ");
            }
        } else if (commandPart1.equalsIgnoreCase("want") && parts.length == 2) {
            String commandPart2 = parts[1];
            if (commandPart2.equalsIgnoreCase("rest")) {
                coffeeMachine.isCleanNeed();
                coffeeMachine.manyCappuccino(3);
                Date date34 = new Date();
                drinkLog.add("three cups of cappuccino " + date34);
                log.info("make 3 cups of cappuccino ");
                logger.addState("make 3 cups of cappuccino ");
            } else if (commandPart2.equalsIgnoreCase("sleep")) {
                coffeeMachine.isCleanNeed();
                coffeeMachine.manyEspresso(3);
                Date date34 = new Date();
                drinkLog.add("three cups of espresso " + date34);
                log.info("make 3 cups of espresso ");
                logger.addState("make 3 cups of espresso ");
            }
        } else if (commandPart1.equalsIgnoreCase("recipe") && parts.length == 2) {
            String commandPart2 = parts[1];
            if (commandPart2.matches("espresso")) {
                coffeeMachine.espressoRec();
                logger.addState("recipe espresso");
            } else if (commandPart2.matches("cappuccino")) {
                coffeeMachine.cappuccinoRec();
                logger.addState("recipe cappuccino");
            }

        } else if (commandPart1.equals("macros") && parts.length == 2) {

            String macrosName = parts[1];
            String bas = sc.nextLine();
            profiles.put(macrosName, bas);
            logger.addState("create macros " + parts[1]);
//
        } else if (commandPart1.equals("use") && parts.length == 3) {
            String macros = parts[1];
            if (macros.equals("macros")) {
                String macrosName = parts[2];
                String commands = profiles.get(macrosName);
                String[] com = commands.split(", ");
                for (String words : com) {
                    comScan(words);
                }
            }
            }

        }

    }
