import lombok.Setter;

public class CoffeeMachine extends Technics implements Power {


    public boolean on;
    public boolean noNeed;
    public double water;
    public double coffee;
    public double milk;
    public int rubbish;
    public final int coffeeNeedEsp = 5;
    public final int waterNeedEsp = 30;
    public final int rubbishEsp = 2;
    public final int coffeeNeedCap = 10;
    public final int waterNeedCap = 200;
    public final int milkNeedCap = 20;
    public final int rubbishCap = 5;
    public final int minWater = 30;
    //------------параметры баков-------
    @Setter int maxWater;
    public final int minMilk = 10;
    @Setter int maxMilk;
    public final int minCoffee = 10;
    @Setter int maxCoffee;
    public final int maxRubbish = 100;
    //----------------------------------


    public CoffeeMachine() {
        water = 3000.0 * Math.random();
        coffee = 500.0 * Math.random();
        milk = 1000.0 * Math.random();
        rubbish = 0;
        on = false;

    }


    @Override
    public void powerOn() {
        on = true;
    }

    @Override
    public void powerOff() {
        System.out.println("See you soon");
        on = false;
    }


    public void addWater(Double amount) {
        if (on) {
            if (water + amount <= maxWater()) {
                water = water + amount;
                System.out.println("добавлено " + amount + " мл воды");
                getWaterStatus();
            } else
                System.out.println("🐼не получится добавить " + amount + " мл воды🐼");

        }
    }

    public void addMilk(Double amount) {
        if (on) {
            if (milk + amount <= maxMilk()) {
                milk = milk + amount;
                System.out.println("добавлено " + amount + " мл молока");
                getMilkStatus();
            } else
                System.out.println("🐼не получится добавить" + amount + " мл молока🐼");

        }
    }

    public void addCoffee(Double amount) {
        if (on)
            if ((coffee + amount) <= maxCoffee()) {
                coffee = coffee + amount;
                System.out.println("добавлено " + amount + " гр кофе");
                getCoffeeStatus();
            } else
                System.out.println("🐼не получится добавить" + amount + " гр кофе🐼");


    }


    private double maxCoffee() {
        return maxCoffee;
    }

    private double maxMilk() {
        return maxMilk;
    }

    private double maxWater() {
        return maxWater;
    }

    public void espresso() {
        if (on && noNeed) {
            if ((waterNeedEsp <= water) && (coffeeNeedEsp <= coffee) && (rubbish < maxRubbish)) {
                System.out.println("🐼Эспрессо готов🐼");
                water = water - waterNeedEsp;
                coffee = coffee - coffeeNeedEsp;
                rubbish = rubbish + rubbishEsp;
                getCoffeeStatus();
                getWaterStatus();
                getRubbishStatus();

            } else
                System.out.println("Недостаточно ингредиентов для приготовления эспрессо");
        }
    }

    public void espressoRec() {
        if (on) {
            System.out.println("🐼🐼" + CoffeeTypes.ESPRESSO.getReceipt());
        }
    }

    public void cappuccinoRec() {
        if (on) {
            System.out.println("🐼🐼" + CoffeeTypes.CAPPUCCINO.getReceipt());
        }
    }

    public void cappuccino() {
        if (on && noNeed) {
            if ((waterNeedCap <= water) && (coffeeNeedCap <= coffee) && (milkNeedCap <= milk) && (rubbish < maxRubbish)) {
                System.out.println("🐼Капучино готов🐼");
                water = water - waterNeedCap;
                coffee = coffee - coffeeNeedCap;
                milk = milk - milkNeedCap;
                rubbish = rubbish + rubbishCap;
                getWaterStatus();
                getCoffeeStatus();
                getMilkStatus();
                getRubbishStatus();
            } else
                System.out.println("Недостаточно ингредиентов для капучино");
        }
    }

    public void manyCappuccino(int amount) {
        if (on && noNeed) {
            for (int i = 1; i <= amount; i++)
                if ((waterNeedCap <= water) && (coffeeNeedCap <= coffee) && (milkNeedCap <= milk) && (rubbish < maxRubbish)) {
                    System.out.println("🐼Ваша🐼" + i + " кружка капучино готова");
                    water = water - waterNeedCap;
                    coffee = coffee - coffeeNeedCap;
                    milk = milk - milkNeedCap;
                    rubbish = rubbish + rubbishCap;
                    getWaterStatus();
                    getCoffeeStatus();
                    getMilkStatus();
                    getRubbishStatus();
                } else {
                    System.out.println("выполните проверку состояния ингредиентов и отходов");
                }

        } else
            System.out.println("🐼Необходимо произвести очистку мусора🐼");
    }

    public void manyEspresso(int amount) {
        if (on && noNeed) {
            for (int i = 1; i <= amount; i++)
                if ((waterNeedEsp <= water) && (coffeeNeedEsp <= coffee) && (rubbish < maxRubbish)) {
                    System.out.println("🐼Ваша🐼" + i + " кружка эспрессо готова");
                    water = water - waterNeedEsp;
                    coffee = coffee - coffeeNeedEsp;
                    rubbish = rubbish + rubbishEsp;
                    getWaterStatus();
                    getCoffeeStatus();
                    getRubbishStatus();
                } else {
                    System.out.println("выполните проверку состояния ингредиентов и отходов");
                }

        } else {
            System.out.println("🐼Необходимо произвести очистку мусора🐼");
        }
    }
    public void getWaterStatus() {
        if (on) {
            if (water <= minWater) {
                System.out.println("🐼нужно долить воды🐼");
            } else if ((water > maxWater)) {
                System.out.println("🐼Перелив🐼!" + water + "");
            } else {
                System.out.println("🐼 сейчас в баке 🐼" + water + " мл воды");
            }
        }
    }

    public void getCoffeeStatus() {
        if (on) {
            if ((coffee <= minCoffee)) {
                System.out.println("🐼 Нужно больше золо...кофе)🐼");
            } else if ((coffee > maxCoffee)) {
                System.out.println("🐼OVERDOSE🐼");
            } else {
                System.out.println("🐼 сейчас в баке " + coffee + " гр кофе 🐼");
            }
        }
    }

    public void getMilkStatus() {
        if (on) {
            if (milk <= minMilk) {
                System.out.println("🐼 необходимо добавить молоко🐼");
            } else if (milk > maxMilk) {
                System.out.println("🐼слишком много молока🐼");
            } else {
                System.out.println("🐼сейчас в баке " + milk + " мл молока 🐼");
            }
        }
    }

    public void getRubbishStatus() {
        if (on) {
            if (rubbish >= maxRubbish) {
                System.out.println("🐼Необходимо произвести очистку мусора🐼");
            } else {
                System.out.println("🐼 сейчас бак с отходами заполнен на 🐼 " + rubbish + " %");
            }
        }
    }

    public void cleanRubbish() {
        if (on) {
            if (rubbish >= maxRubbish / 2) {
                rubbish = 0;
                System.out.println("🐼Бак с отходами очищен🐼");
            } else {
                System.out.println("🐼 сейчас бак с отходами заполнен на 🐼 " + rubbish + " % и его еще рано чистить");
            }
        }
    }

    public void isCleanNeed() {
        noNeed = rubbish < maxRubbish;
    }

}
